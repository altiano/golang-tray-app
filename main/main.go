package main

import (
	"fmt"
	"reflect"
	"strconv"
)

type Test struct {
}

func (t *Test) SayHello(name string, d int, s Test) bool {

	return true
}

func FuncAnalyse(m interface{}) {

	//Reflection type of the underlying data of the interface
	x := reflect.TypeOf(m)

	numIn := x.NumIn()   //Count inbound parameters
	numOut := x.NumOut() //Count outbounding parameters

	fmt.Println("Method:", x.String())
	fmt.Println("Variadic:", x.IsVariadic()) // Used (<type> ...) ?
	fmt.Println("Package:", x.PkgPath())

	for i := 0; i < numIn; i++ {

		inV := x.In(i)
		in_Kind := inV.Kind() //func
		fmt.Printf("\nParameter IN: "+strconv.Itoa(i)+"\nKind: %v\nName: %v\n-----------", in_Kind, inV.Name())
	}
	for o := 0; o < numOut; o++ {

		returnV := x.Out(0)
		return_Kind := returnV.Kind()
		fmt.Printf("\nParameter OUT: "+strconv.Itoa(o)+"\nKind: %v\nName: %v\n", return_Kind, returnV.Name())
	}

	xx := reflect.ValueOf(x)
	xx.Call([]reflect.Value{})

}

func main() {

	// var n *Test = &Test{}
	// methodValue := n.SayHello

	// FuncAnalyse(methodValue)

	j := &T{}
	var t interface{} = j.Geeks

	xx := reflect.ValueOf(t)
	xx.Call([]reflect.Value{
		reflect.ValueOf(int(2)),
	})

	// var t T
	// reflect.ValueOf(&t).MethodByName("Geeks").Call([]reflect.Value{})

}

type T struct{}

func (t *T) Geeks(a int) {
	fmt.Println("GeekforGeeks", a)
}
