package textlogger

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"golang-tray-app/src/shared"
)

const title = "textlogger.Control()"
const logFolderPath = "./logs"
const logFilePath = logFolderPath + "/result.log"

// Control .
func Control(cnator *shared.Coordinator) {
	fmt.Println(title)

	// writer to stdout & logfile
	logFile := getLogFile()
	mw := io.MultiWriter(os.Stdout, logFile)

	// print current time
	fmt.Fprintln(mw, time.Now().Format(time.Stamp))

	// iterate until updateStock is closed
	for stockConfig := range cnator.UpdateStock {
		fmt.Fprintln(mw, stockConfig.Stock, stockConfig.Price)
	}
}

func getLogFile() *os.File {
	config := shared.NewConfig()

	// ensure logs folder created
	path := filepath.Join(config.ExecDir, logFolderPath)
	if err := os.MkdirAll(path, os.ModePerm); err != nil {
		panic(err)
	}

	// append to existing log file
	logpath := filepath.Join(config.ExecDir, logFilePath)
	logFile, err := os.OpenFile(logpath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}

	return logFile
}
