package src

import (
	"golang-tray-app/src/cnator"
	"golang-tray-app/src/settingswindow"
	"golang-tray-app/src/shared"
	"golang-tray-app/src/tray"
)

// Run .
func Run() {
	//
	channels := shared.NewCordinator()
	cnator := cnator.NewCnator()

	sWindow := settingswindow.New(
		channels.OpenSWindow,
	)

	cnator.Subscribe(channels.OpenSWindow, sWindow.Show)
	cnator.Serve()

	tray := tray.New(
		// this must be send-only channel
		// NOTE this is a must
		// bring this up in the article
		channels.OpenSWindow,
	)
	tray.Run()

	<-channels.ProgTerm
}
