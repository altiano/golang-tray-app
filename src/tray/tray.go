package tray

import (
	"golang-tray-app/src/tray/icon"

	"github.com/getlantern/systray"
)

type (
	tray struct {
		events events
		menus  menus
	}

	menus struct {
		lastUpdate *systray.MenuItem
		settings   *systray.MenuItem
	}

	events struct {
		openSWindow chan<- struct{}
	}
)

func New(openSWindow chan<- struct{}) *tray {
	return &tray{
		events: events{
			openSWindow: openSWindow,
		},
	}
}

func (t *tray) Run() {
	systray.Run(func() {

		t.init()
		t.createSettingsMenu()
		t.createQuitMenu()
		systray.AddSeparator()
		t.createLastUpdate()
		systray.AddSeparator()

	}, func() {})
}

func (t *tray) init() {
	systray.SetTemplateIcon(icon.Stock, icon.Stock)
	systray.SetTitle("The title")
	systray.SetTooltip("The tooltip")
}

func (t *tray) createSettingsMenu() {
	t.menus.settings = systray.AddMenuItem("Settings", "")

	go func() {
		for {
			t.events.openSWindow <- <-t.menus.settings.ClickedCh
		}
	}()

	// t.menus.settings.Disable()
}

func (t *tray) createQuitMenu() {
	mQuitOrig := systray.AddMenuItem("Quit", "Quit the whole app")
	go func() {
		<-mQuitOrig.ClickedCh
		// if t.settingsWindowRunner.proc != nil {
		// 	t.settingsWindowRunner.proc.Process.Kill()
		// }
		systray.Quit()
	}()
}

func (t *tray) createLastUpdate() {
	t.menus.lastUpdate = systray.AddMenuItem("Last Update: -", "")
	t.menus.lastUpdate.Disable()
}
