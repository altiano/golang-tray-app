package tray

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"golang-tray-app/src/shared"
	guiShared "golang-tray-app/src/shared/gui"

	tianjson "gitlab.com/altiano/goutils/encoding/json"
)

type settingsWindowWatcher struct {
	cnator      *shared.Coordinator
	list        []shared.StockConfig
	appSettings shared.AppSettings
	reader      io.Reader
	writer      io.Writer
}

func (w *settingsWindowWatcher) watch() {
	sc := bufio.NewScanner(w.reader)
	for sc.Scan() {
		// receive
		text := sc.Text()

		// parsing
		input := guiShared.Command{}
		if err := json.Unmarshal([]byte(text), &input); err != nil {
			fmt.Fprintln(os.Stdout, "error parsing", text)
			continue
		}

		// routing
		switch input.Type {
		case guiShared.ClientResponse:
			fmt.Fprintln(os.Stdout, guiShared.ClientResponse, "missing implementation")

		case guiShared.ClientLog:
			clientLog := guiShared.LogContent{}
			if err := json.Unmarshal([]byte(input.Content.(string)), &clientLog); err != nil {
				fmt.Fprintln(os.Stdout, "log", err)
			} else {
				fmt.Fprintln(os.Stdout, "log", clientLog.Scope, clientLog.Msg)
			}

		default:
			output := w.routeReplyToClienCommand(input)

			bytes := tianjson.Marshal(output)
			payloadString := string(bytes)

			fmt.Fprintln(os.Stdout, "sending", payloadString)
			fmt.Fprintln(w.writer, payloadString)
		}
	}
}

func (w *settingsWindowWatcher) routeReplyToClienCommand(input guiShared.Command) guiShared.Command {
	commandResponse := guiShared.Command{}

	fmt.Fprintln(os.Stdout, "receiving clientCommand", input.Type)

	switch input.Type {
	case guiShared.ClientListStock:
		content := w.list
		contentBytes, _ := json.Marshal(content)

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: string(contentBytes),
		}

	case guiShared.ClientAppSettings:
		content := w.appSettings
		contentBytes, _ := json.Marshal(content)

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: string(contentBytes),
		}

	case guiShared.ClientCreateStock:

		request := shared.CreateStockRequest{
			ErrRes: make(chan error),
		}
		tianjson.Unmarshal([]byte(input.Content.(string)), &request)

		w.cnator.CreateStock <- request

		response := ""
		if err := <-request.ErrRes; err != nil {
			response = err.Error()
		}

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: response,
		}

	case guiShared.ClientUpdateStockAlert:

		request := shared.UpdateStockAlertRequest{}
		tianjson.Unmarshal([]byte(input.Content.(string)), &request)

		req := shared.UpdateStockRequestOrigin{
			StockName: request.Stock,
			UpdatedData: map[string]interface{}{
				"AlertAtMax": request.AlertAtMax,
				"AlertAtMin": request.AlertAtMin,
			},
			ErrRes: make(chan error),
		}
		w.cnator.UpdateStockOrigin <- req

		response := ""
		if err := <-req.ErrRes; err != nil {
			response = err.Error()
		}

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: response,
		}

	case guiShared.ClientUpdateAppSettings:

		req := shared.UpdateAppSettingsRequest{
			ErrRes: make(chan error),
		}
		tianjson.Unmarshal([]byte(input.Content.(string)), &req)

		w.cnator.UpdateAppSettings <- req

		response := ""
		if err := <-req.ErrRes; err != nil {
			response = err.Error()
		}

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: response,
		}

	case guiShared.ClientRemoveStock:

		request := shared.RemoveStockRequest{
			ErrRes: make(chan error),
		}

		tianjson.Unmarshal([]byte(input.Content.(string)), &request)

		w.cnator.RemoveStock <- request

		response := ""
		if err := <-request.ErrRes; err != nil {
			response = err.Error()
		}

		commandResponse = guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: response,
		}
	}

	return commandResponse
}

func (w *settingsWindowWatcher) sendFocusCommand() {
	output := guiShared.Command{
		Type:    guiShared.ServerFocusWindow,
		Content: "",
	}

	bytes := tianjson.Marshal(output)
	payloadString := string(bytes)

	fmt.Fprintln(os.Stdout, "sending", payloadString)
	fmt.Fprintln(w.writer, payloadString)
}
