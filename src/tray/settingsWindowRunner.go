package tray

import (
	"log"
	"os/exec"

	"golang-tray-app/src/shared"
)

type settingsWindowRunner struct {
	cnator      *shared.Coordinator
	list        shared.StockConfigs
	appSettings shared.AppSettings
	proc        *exec.Cmd
	watcher     settingsWindowWatcher
}

func (s *settingsWindowRunner) RefreshList(list shared.StockConfigs) {
	s.list = list
}

func (s *settingsWindowRunner) RefreshAppSettings(appSettings shared.AppSettings) {
	s.appSettings = appSettings
}

func (s *settingsWindowRunner) runGui() {
	if s.proc != nil {
		s.watcher.sendFocusCommand()
		return
	}

	config := shared.NewConfig()
	s.proc = exec.Command(config.GuiPath, "stdin")

	// input
	stdin, err := s.proc.StdinPipe()
	if err != nil {
		panic(err)
	}
	defer stdin.Close()

	// output
	stdout, err := s.proc.StdoutPipe()
	if err != nil {
		panic(err)
	}
	defer stdout.Close()

	s.watcher = settingsWindowWatcher{
		cnator:      s.cnator,
		list:        s.list,
		appSettings: s.appSettings,
		reader:      stdout,
		writer:      stdin,
	}

	go s.watcher.watch()

	// start
	// existing = proc
	if err := s.proc.Run(); err != nil {
		log.Println(err)
	}

	s.proc.Wait()
	s.proc = nil
}
