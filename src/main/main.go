package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	go func() {
		<-ctx.Done()
		fmt.Println("x")
	}()

	go func() {
		<-ctx.Done()
		fmt.Println("xx")
	}()

	go func() {
		<-ctx.Done()
		fmt.Println("xx")
	}()

	<-time.After(3 * time.Second)

}

// func main() {
// 	a := make(chan struct{})
// 	b := make(chan struct{})

// 	go func() {
// 		b <- struct{}{}
// 		a <- struct{}{}
// 		fmt.Println("done")
// 	}()

// 	go func() {

// 		select {
// 		case x := <-a:
// 			fmt.Println(x)
// 		case xx := <-b:
// 			fmt.Println("xs", xx)
// 			go func() {
// 				<-a
// 			}()
// 		}
// 	}()

// 	time.Sleep(5 * time.Second)
// }
