package db

import (
	"sort"

	"golang-tray-app/src/shared"

	"github.com/asdine/storm"
)

func handleGetAppSettings(myDB *storm.DB) shared.AppSettings {
	var appSettings shared.AppSettings
	myDB.One("ID", 1, &appSettings)

	if appSettings.ID == 0 {
		appSettings = shared.AppSettings{
			ID:              1,
			LaunchAtStartup: true,
		}
		if err := myDB.Save(&appSettings); err != nil {
			panic(err)
		}
	}

	return appSettings
}

func handleUpdateAppSettings(myDB *storm.DB, req shared.UpdateAppSettingsRequest) error {
	return myDB.UpdateField(&shared.AppSettings{ID: 1}, "LaunchAtStartup", req.LaunchAtStartup)
}

func handleListStock(myDB *storm.DB) []shared.StockConfig {
	var list []shared.StockConfig

	if err := myDB.All(&list); err != nil {
		panic(err)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].Stock < list[j].Stock
	})

	return list
}

func handleCreateStock(myDB *storm.DB, stock shared.CreateStockRequest) error {
	user := shared.StockConfig{
		Stock: stock.Name,
	}

	return myDB.Save(&user)
}

func handleRemoveStock(myDB *storm.DB, stock shared.RemoveStockRequest) error {
	s := shared.StockConfig{}
	myDB.One("Stock", stock.Name, &s)

	return myDB.DeleteStruct(&s)
}

func handleUpdateStock(myDB *storm.DB, stock shared.UpdateStockRequestOrigin) (shared.StockConfig, error) {

	for k, v := range stock.UpdatedData {
		err := myDB.UpdateField(&shared.StockConfig{Stock: stock.StockName}, k, v)

		if err != nil {
			return shared.StockConfig{}, err
		}
	}

	s := shared.StockConfig{}
	err := myDB.One("Stock", stock.StockName, &s)

	return s, err
}
