package db

import (
	"os"
	"path/filepath"

	"golang-tray-app/src/shared"

	"github.com/asdine/storm"
)

func setup() *storm.DB {
	db := ensureDBCreated()
	return db
}

func ensureDBCreated() *storm.DB {
	config := shared.NewConfig()

	fullDir := append([]string{config.ExecDir}, config.DBPath...)
	dbPath := filepath.Join(fullDir...)
	os.MkdirAll(dbPath, os.ModePerm)

	myDB, err := storm.Open(filepath.Join(dbPath, "stockNotifier.db"))
	if err != nil {
		panic(err)
	}

	return myDB
}
