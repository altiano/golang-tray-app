package db

import (
	"fmt"
	"os/exec"

	"golang-tray-app/src/shared"
)

// Control .
func Control(cnator *shared.Coordinator) {
	db := setup()
	cnator.StockList <- handleListStock(db)
	appSettings := handleGetAppSettings(db)
	cnator.AppSettings <- appSettings
	ensureAppSettings(appSettings)

	for {
		var err error
		select {
		case req := <-cnator.UpdateAppSettings:
			err = handleUpdateAppSettings(db, req)
			sendErrorResponse(req.ErrRes, err)

			if err == nil {
				appSettings := handleGetAppSettings(db)
				ensureAppSettings(appSettings)
				cnator.AppSettings <- appSettings
			}

		case req := <-cnator.CreateStock:
			err = handleCreateStock(db, req)
			sendErrorResponse(req.ErrRes, err)

			if err == nil {
				cnator.StockList <- handleListStock(db)
			}

		case req := <-cnator.UpdateStockOrigin:
			updatedStock, err := handleUpdateStock(db, req)
			sendErrorResponse(req.ErrRes, err)

			if err == nil {
				cnator.UpdateStockItem <- updatedStock
			}

		case stock := <-cnator.RemoveStock:
			err = handleRemoveStock(db, stock)
			sendErrorResponse(stock.ErrRes, err)

			if err == nil {
				// NOTE should not re trigger list,
				// list is expensive
				cnator.StockList <- handleListStock(db)
			}
		}
	}
}

func sendErrorResponse(errRes chan<- error, err error) {
	if errRes != nil {
		errRes <- err
	}
}

func ensureAppSettings(appSettings shared.AppSettings) {
	config := shared.NewConfig()
	if appSettings.LaunchAtStartup {
		c := fmt.Sprintf(
			`tell application "System Events" to make login item at end with properties {name: "%v", path:"%v", hidden:true}`,
			config.StartupExeName,
			config.StartupExeLoc,
		)

		exec.Command("osascript", "-e", c).Run()
	} else {
		exec.Command(
			"/usr/bin/osascript",
			"-e",
			fmt.Sprintf(
				`tell application "System Events" to delete login item "%v"`,
				config.StartupExeName,
			),
		).Run()
	}
}
