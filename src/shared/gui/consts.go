package gui

const (
	StockListType            = "StockList"
	StockListRouteRegistered = "StockListRouteRegistered"

	// client send
	// [user-defined]
	ClientListStock         = "ListStock"
	ClientCreateStock       = "CreateStock"
	ClientUpdateStockAlert  = "UpdateStockAlert"
	ClientRemoveStock       = "RemoveStock"
	ClientAppSettings       = "AppSettings"
	ClientUpdateAppSettings = "UpdateAppSettings"
	// [system-defined]
	ClientResponse = "ClientResponse"
	ClientLog      = "ClientLog"

	// server sends
	// [user-defined]
	ServerFocusWindow = "FocusWindow"
	// ServerXxxx2Cmd
	// ServerBringToFrontCmd
	// [system-defined]
	ServerResponse = "ServerResponse"
)
