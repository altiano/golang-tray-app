package gui

type (
	Coordinator struct {
		ClientRouting  chan ClientRouting
		ClientCommand  chan Command
		ClientResponse chan string
		ClientLog      chan LogContent
		ClientAddRoute chan AddNewServerRoute

		ServerRouting  chan ServerRouting
		ServerCommand  chan Command
		ServerResponse chan string
	}

	AddNewServerRoute struct {
		RouteName     string
		ServerRouting chan ServerRouting
	}

	ServerRouting struct {
		Command
		Response chan string
	}

	ClientRouting struct {
		Command
		WithResponse interface{}
		Error        chan error
	}

	Command struct {
		Type    string      `json:"type"`
		Content interface{} `json:"content"`
	}

	LogContent struct {
		Scope string `json:"scope"`
		Msg   string `json:"msg"`
	}
)
