package shared

type (
	StockConfigs    []StockConfig
	StockConfigList []StockConfigRuntime
)

func (slist StockConfigs) ToRuntimeList() StockConfigList {
	newList := make([]StockConfigRuntime, len(slist))
	for i, item := range slist {
		newList[i] = StockConfigRuntime{
			StockConfig: item,
		}
	}

	return newList
}

func (slist StockConfigs) FindIndex(name string) (index int) {
	for index, item := range slist {
		if item.Stock == name {
			return index
		}
	}

	return -1
}

func (slist StockConfigs) Update(updatedStock StockConfig) {
	index := slist.FindIndex(updatedStock.Stock)
	if index < 0 {
		return
	}

	slist[index] = updatedStock
}

func (slist StockConfigList) FindIndex(name string) (index int) {
	for index, item := range slist {
		if item.Stock == name {
			return index
		}
	}

	return -1
}

func (slist StockConfigList) Find(name string) (StockConfigRuntime, bool) {
	index := slist.FindIndex(name)
	if index < 0 {
		return StockConfigRuntime{}, false
	}

	return slist[index], true
}

func (slist StockConfigList) Update(updatedStock StockConfig) bool {
	index := slist.FindIndex(updatedStock.Stock)
	if index < 0 {
		return false
	}

	changed := slist[index].StockConfig.HasDiffVal(updatedStock)
	slist[index].StockConfig = updatedStock

	return changed
}

func (slist StockConfigList) UpdatePrice(updatedStock UpdateStockRequest) {
	index := slist.FindIndex(updatedStock.Stock)
	if index < 0 {
		return
	}

	slist[index].StockPrice = updatedStock.Price
	slist[index].Error = updatedStock.Error
}
