package shared

import (
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Coordinator .
type Coordinator struct {
	originCnator      *Coordinator
	ProgTerm          <-chan os.Signal
	StockList         chan StockConfigs
	stockListSubs     []chan StockConfigs
	UpdateStockOrigin chan UpdateStockRequestOrigin
	UpdateStockItem   chan StockConfig        // This should be updated stock, after DB update, also should be just StockConfig
	UpdateStock       chan UpdateStockRequest // This is update for runtime config : current price
	LastBatchUpdate   chan time.Time          // This is stockPrice batch
	updateStockSubs   []chan UpdateStockRequest
	CreateStock       chan CreateStockRequest
	createStockSubs   []chan CreateStockRequest
	RemoveStock       chan RemoveStockRequest
	removeStockSubs   []chan RemoveStockRequest

	AppSettings       chan AppSettings
	UpdateAppSettings chan UpdateAppSettingsRequest

	//
	OpenSettingsWindow  chan struct{}
	CloseSettingsWindow chan struct{}

	OpenSWindow chan struct{}
}

// UpdateStockSubscriptions []chan StockConfig

// Controller .
type Controller func(Coordinator)

// NewCordinator .
func NewCordinator() *Coordinator {
	cnator := Coordinator{
		ProgTerm:          getProgTermSignal(),
		StockList:         make(chan StockConfigs),
		CreateStock:       make(chan CreateStockRequest),
		UpdateStockOrigin: make(chan UpdateStockRequestOrigin),
		UpdateStockItem:   make(chan StockConfig),
		UpdateStock:       make(chan UpdateStockRequest),
		RemoveStock:       make(chan RemoveStockRequest),
		LastBatchUpdate:   make(chan time.Time),
		UpdateAppSettings: make(chan UpdateAppSettingsRequest),
		AppSettings:       make(chan AppSettings),
		OpenSWindow:       make(chan struct{}),
	}

	return &cnator
}

func getProgTermSignal() chan os.Signal {
	progTerm := make(chan os.Signal, 1)
	signal.Notify(progTerm, os.Interrupt, syscall.SIGTERM)
	return progTerm
}
