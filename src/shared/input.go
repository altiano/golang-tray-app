package shared

type Input struct {
	Type    string `json:"type"`
	Content string `json:"content"`
}
