package shared

// StockConfig .
type (
	StockConfig struct {
		Stock        string `json:"stock" storm:"id"`
		AlertAtMin   int    `json:"alertAtMin"`
		AlertAtMax   int    `json:"alertAtMax"`
		LastDayPrice int    `json:"lastDayPrice"`
	}

	StockConfigRuntime struct {
		StockConfig
		StockPrice int
		Error      string `json:"-"`
	}

	// CreateStockRequest .
	// PATTERN: requestResponse in channel
	CreateStockRequest struct {
		Name   string `json:"name"`
		ErrRes chan error
	}

	// RemoveStockRequest .
	RemoveStockRequest struct {
		Name   string
		ErrRes chan error
	}

	// UpdateStockRequest . // should be updatedStockPrice/Request
	UpdateStockRequest struct {
		Stock string
		Price int
		Error string
	}

	// UpdateStockAlertRequest .
	UpdateStockAlertRequest struct {
		Stock      string
		AlertAtMax int
		AlertAtMin int
	}

	// UpdateStockRequestOrigin .
	UpdateStockRequestOrigin struct {
		StockName   string
		UpdatedData map[string]interface{}
		ErrRes      chan error
	}

	// UpdateAppSettingsRequest .
	UpdateAppSettingsRequest struct {
		LaunchAtStartup bool
		ErrRes          chan error
	}
)

// GetPlusMinus .
func (s StockConfigRuntime) GetPlusMinus() float32 {
	diff := float32(s.LastDayPrice) / float32(s.StockPrice)
	asPercentage := diff * 100
	diffPercentarge := 100 - asPercentage
	return diffPercentarge
}

func (s StockConfig) HasDiffVal(target StockConfig) bool {
	return s.AlertAtMax != target.AlertAtMax || s.AlertAtMin != target.AlertAtMin
}
