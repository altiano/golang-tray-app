package shared

import (
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

// static
var (
	config Config
	once   sync.Once
)

// Config .
type Config struct {
	ExecDir        string
	DBPath         []string      `envconfig:"DB_PATH" required:"true"`
	DBDir          string        `envconfig:"DB_DIR" required:"true"`
	StockListPath  string        `envconfig:"STOCK_LIST_PATH" required:"true"`
	ScrapInterval  time.Duration `envconfig:"SCRAP_INTERVAL" required:"true"`
	StartupExeLoc  string        `envconfig:"STARTUP_EXE_LOC" required:"true"`
	StartupExeName string        `envconfig:"STARTUP_EXE_NAME" required:"true"`
	GuiPath        string        `envconfig:"GUI_PATH" required:"true"`
}

// NewConfig .
func NewConfig() Config {
	once.Do(func() {
		execPath, err := os.Executable()
		if err != nil {
			panic(err)
		}

		if len(os.Args) > 0 && strings.Contains(os.Args[0], "/main") {
			execPath = "./main.go"
		}

		execDir := filepath.Join(execPath, "..")

		filename := filepath.Join(execDir, ".env")

		file, err := os.Stat(filename)

		if err != nil && !os.IsNotExist(err) {
			panic(err)
		}

		if file != nil {
			if err := godotenv.Load(filename); err != nil {
				panic(err)
			}
		}

		if err := envconfig.Process("", &config); err != nil {
			panic(err)
		}

		config.ExecDir = execDir
	})

	return config
}
