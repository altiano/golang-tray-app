package shared

import "fmt"

/*
POC
	0.ChannelA
	0.SubA(1) : 0 -> ChannelA1:1
	0.SubA(2) : 0 -> ChannelA2:2

	1.ChannelB == 0.ChannelB
	0.SubB(2) : 1 -> ChannelB1:2

	2.ChannelC == 0.ChannelC
	0.SubC(1) : 2 -> ChannelC1:1
*/

// Clone .
func (cnator *Coordinator) Clone() *Coordinator {
	newCnator := *cnator
	newCnator.originCnator = cnator
	return &newCnator
}

// NotifyStockList .
func (cnator *Coordinator) NotifyStockList() *Coordinator {
	cnator.StockList = make(chan StockConfigs)

	cnator.originCnator.stockListSubs = append(
		cnator.originCnator.stockListSubs,
		cnator.StockList)

	return cnator
}

// NotifyUpdateStock .
func (cnator *Coordinator) NotifyUpdateStock() *Coordinator {
	cnator.UpdateStock = make(chan UpdateStockRequest)

	cnator.originCnator.updateStockSubs = append(
		cnator.originCnator.updateStockSubs,
		cnator.UpdateStock)

	return cnator
}

// NotifyCreateStock .
func (cnator *Coordinator) NotifyCreateStock() *Coordinator {
	cnator.CreateStock = make(chan CreateStockRequest)

	cnator.originCnator.createStockSubs = append(
		cnator.originCnator.createStockSubs,
		cnator.CreateStock)

	return cnator
}

// NotifyRemoveStock .
func (cnator *Coordinator) NotifyRemoveStock() *Coordinator {
	cnator.RemoveStock = make(chan RemoveStockRequest)

	cnator.originCnator.removeStockSubs = append(
		cnator.originCnator.removeStockSubs,
		cnator.RemoveStock)

	return cnator
}

// ServeStockList .
func (cnator *Coordinator) Serve() {
	fmt.Println("coordinator.Serve()")
	go func() {
		for i := range cnator.StockList {
			for _, c := range cnator.stockListSubs {
				c <- i
			}
		}
	}()
	go func() {
		for i := range cnator.UpdateStock {
			for _, c := range cnator.updateStockSubs {
				c <- i
			}
		}
	}()
	go func() {
		for i := range cnator.CreateStock {
			for _, c := range cnator.createStockSubs {
				c <- i
			}
		}
	}()
}
