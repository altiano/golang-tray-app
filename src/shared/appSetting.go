package shared

// AppSettings .
type (
	AppSettings struct {
		ID              int
		LaunchAtStartup bool `json:"launchAtStartup"`
	}
)
