package settingswindow

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"runtime/debug"

	"golang-tray-app/src/shared"
	guiShared "golang-tray-app/src/shared/gui"
)

type (
	settingsWindow struct {
		list   []shared.StockConfig
		proc   *process
		events events
	}

	events struct {
		AddNewStock chan shared.CreateStockRequest
	}
)

func New(openSWindow chan<- struct{}) settingsWindow {
	return settingsWindow{
		events: events{
			AddNewStock: make(chan shared.CreateStockRequest),
		},
	}
}

func (s *settingsWindow) Show() {
	fmt.Println("show the window")
	// if s.proc != nil {
	// 	cmd := s.sendFocusCommand()
	// 	s.proc.tell(cmd)

	// 	return
	// }

	// if s.proc = spawn(); s.proc == nil {
	// 	return
	// }

	// // TODO
	// s.proc.listen(guiShared.ClientCreateStock, s.handleCreateStock)
	// // s.proc.listen(guiShared.ClientLog, s.handleClientLog)

	// s.proc.runAndWait()
	// s.proc = nil
}

func (s *settingsWindow) close() {

}

//
//
//
func (s *settingsWindow) handleCreateStock(input guiShared.Command) guiShared.Command {
	log := log.New(os.Stdout, "handleCreateStock()", log.Ltime)

	// should not this pattern
	request := shared.CreateStockRequest{
		ErrRes: make(chan error),
	}

	// NOTE should have timeout on GUI
	// NOTE should handle error as different command type?

	if err := json.Unmarshal([]byte(input.Content.(string)), &request); err != nil {
		log.Println(err, string(debug.Stack()))
		return guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: err.Error(),
		}
	}

	// NOTE probably should make a helper function for this
	s.events.AddNewStock <- request

	if err := <-request.ErrRes; err != nil {
		return guiShared.Command{
			Type:    guiShared.ServerResponse,
			Content: err.Error(),
		}

	}

	// NOTE empty string means success, for now.
	return guiShared.Command{
		Type:    guiShared.ServerResponse,
		Content: "",
	}
}

func (s *settingsWindow) sendFocusCommand() guiShared.Command {
	return guiShared.Command{
		Type:    guiShared.ServerFocusWindow,
		Content: "",
	}
}
