package settingswindow

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"

	"golang-tray-app/src/shared"
	guiShared "golang-tray-app/src/shared/gui"

	tianjson "gitlab.com/altiano/goutils/encoding/json"
)

type (
	process struct {
		proc      *exec.Cmd
		listeners map[string]listenerFunc
	}

	listenerFunc func(guiShared.Command) guiShared.Command
)

func spawn() *process {

	config := shared.NewConfig()
	proc := exec.Command(config.GuiPath, "stdin")

	return &process{
		proc: proc,
	}
}

func (p *process) runAndWait() {
	log := log.New(os.Stdout, "runAndWait()", log.Ltime)

	// input
	stdin, err := p.proc.StdinPipe()
	if err != nil {
		log.Println("p.proc.StdinPipe()", err)
		return
	}
	defer stdin.Close()

	// output
	stdout, err := p.proc.StdoutPipe()
	if err != nil {
		log.Println("p.proc.StdoutPipe()", err)
		return
	}
	defer stdout.Close()

	go p.watch(stdin, stdout)

	// start
	if err := p.proc.Run(); err != nil {
		log.Println(err)
	}

	// NOTE is this necessary?
	// NEED to see the example from golang
	p.proc.Wait()
}

func (p *process) watch(stdin io.Writer, stdout io.Reader) {
	log := log.New(os.Stdout, "watch()", log.Ltime)

	sc := bufio.NewScanner(stdout)
	for sc.Scan() {
		// receive
		text := sc.Text()

		// parsing
		input := guiShared.Command{}
		if err := json.Unmarshal([]byte(text), &input); err != nil {
			log.Println("error parsing", text)
			continue
		}

		// routing
		listener, ok := p.listeners[input.Type]
		if !ok {
			log.Println("missing listener: ", input.Type)
			return
		}

		response := listener(input)

		// send response back
		p.tell(response)
	}
}

func (p *process) tell(output guiShared.Command) {
	bytes := tianjson.Marshal(output)
	payloadString := string(bytes)

	fmt.Fprintln(os.Stdout, "sending", payloadString)
	fmt.Fprintln(p.proc.Stdout, payloadString)

}

func (p *process) listen(command string, handler listenerFunc) {
	p.listeners[command] = handler
}
