package stockprice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"golang-tray-app/src/shared"
)

type (
	// IdxAPI .
	IdxAPI struct{}
	// responseBody .
	responseBody struct {
		ChartData []struct {
			Date   UnixTime
			XLabel string
			Close  float32
		}
		SecurityCode string
		Period       string
		StartDate    UnixTime
		EndDate      UnixTime
		OpenPrice    float32
		MaxPrice     float32
		MinPrice     float32
		Step         float32
	}
)

// Get .
func (api IdxAPI) Get(stock shared.StockConfig) shared.StockConfigRuntime {
	req, _ := http.NewRequest("GET", api.getURL(stock.Stock), nil)
	scr := shared.StockConfigRuntime{
		StockConfig: stock,
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		scr.Error = err.Error()
		return scr
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		scr.Error = err.Error()
		return scr
	}

	resBody := responseBody{}
	if err := json.Unmarshal(body, &resBody); err != nil {
		fmt.Println(string(body))
		scr.Error = err.Error()
		return scr
	}

	currentPrice := getPriceAtIndex(resBody, -1)
	lastPrice := getPriceAtIndex(resBody, -2)

	cPrice, _ := strconv.Atoi(currentPrice)
	lPrice, _ := strconv.Atoi(lastPrice)

	scr.StockPrice = cPrice
	scr.LastDayPrice = lPrice

	return scr
}

func (IdxAPI) getURL(stockCode string) string {
	return "https://www.idx.co.id/umbraco/Surface/Helper/GetStockChart?indexCode=" + stockCode + "&period=1W"
}

func getPriceAtIndex(resBody responseBody, index int) string {
	latestActiveDayIndex := len(resBody.ChartData) + index
	price := ""
	if latestActiveDayIndex > 0 {
		latestActiveDayChart := resBody.ChartData[latestActiveDayIndex]
		price = strconv.Itoa(int(latestActiveDayChart.Close))
	}
	return price
}
