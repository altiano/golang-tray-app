package stockprice

import (
	"math/rand"
	"time"

	"golang-tray-app/src/shared"
)

type mockApi struct {
}

const min = 300
const max = 600

func (m mockApi) BulkGet(sc []shared.StockConfig) []shared.StockConfigRuntime {

	r := make([]shared.StockConfigRuntime, len(sc))
	for i, s := range sc {
		r[i] = m.Get(s)
	}

	return r
}

func (mockApi) Get(sc shared.StockConfig) shared.StockConfigRuntime {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	lastPrice := r.Intn(max-min+1) + min
	alertAtMin := r.Intn(max-min+1) + min
	alertAtMax := r.Intn(max-alertAtMin+1) + alertAtMin

	scr := shared.StockConfigRuntime{
		StockConfig: sc,
	}
	scr.LastDayPrice = lastPrice
	scr.AlertAtMax = alertAtMax
	scr.AlertAtMin = alertAtMin

	price := r.Intn((max+200)-(alertAtMax-200)) + alertAtMax - 200
	scr.StockPrice = price
	scr.Error = "something"
	return scr
}
