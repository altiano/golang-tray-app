package stockprice

import (
	"testing"

	"golang-tray-app/src/shared"
)

func TestIdxAPI_Get(t *testing.T) {
	type args struct {
		s shared.StockConfig
	}
	tests := []struct {
		name string
		i    IdxAPI
		args args
		want shared.StockConfigRuntime
	}{
		{
			args: args{
				s: shared.StockConfig{
					Stock: "BBCA",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := IdxAPI{}
			if got := i.Get(tt.args.s); got != tt.want {
				t.Errorf("IdxAPI.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
