package stockprice

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"golang-tray-app/src/shared"
)

type (
	SnApi    struct{}
	snApiReq struct {
		Stocks []string
	}
	snApiRes struct {
		Stocks []StockItemRes
	}

	StockItemRes struct {
		Name         string
		CurrentPrice int
		LastDayPrice int
		Error        string
	}
)

func (api SnApi) BulkGet(list []shared.StockConfig) []shared.StockConfigRuntime {
	l := snApiReq{
		Stocks: make([]string, len(list)),
	}

	for i, item := range list {
		l.Stocks[i] = item.Stock
	}

	jsonStr, _ := json.Marshal(l)
	reqBody := bytes.NewBuffer(jsonStr)

	req, _ := http.NewRequest("GET", api.getURL(), reqBody)

	result := make([]shared.StockConfigRuntime, len(list))

	for i := range result {
		result[i].StockConfig = list[i]
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return api.SetErr(result, err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return api.SetErr(result, err)
	}

	resBody := snApiRes{}
	if err := json.Unmarshal(body, &resBody); err != nil {
		return api.SetErr(result, err)
	}

	for _, stockRes := range resBody.Stocks {
		for i, resultItem := range result {
			if resultItem.Stock == stockRes.Name {
				result[i].StockPrice = stockRes.CurrentPrice
				result[i].LastDayPrice = stockRes.LastDayPrice
				result[i].Error = stockRes.Error
			}
		}
	}

	return result
}

func (SnApi) getURL() string {
	return "https://golang-tray-app.altiano.dev/stocks"
}

func (api SnApi) SetErr(result []shared.StockConfigRuntime, err error) []shared.StockConfigRuntime {
	for i, _ := range result {
		result[i].Error = err.Error()
	}

	return result
}
