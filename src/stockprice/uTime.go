package stockprice

import (
	"strconv"
	"time"
)

// UnixTime .
type UnixTime time.Time

// UnmarshalJSON .
func (m *UnixTime) UnmarshalJSON(b []byte) error {

	i, err := strconv.ParseInt(string(b), 10, 64)
	if err != nil {
		return err
	}

	*m = UnixTime(time.Unix(i/1000, (i%1000)*1000*1000))
	return nil
}
