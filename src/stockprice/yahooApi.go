package stockprice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"golang-tray-app/src/shared"
)

type (
	// yahooApi .
	yahooApi struct{}
	// responseBody .
	yahooApiResponse struct {
		Chart struct {
			Result []struct {
				Timestamp  []int `json:"timestamp"`
				Indicators struct {
					Quote []struct {
						High   []float32 `json:"high"`
						Low    []float32 `json:"low"`
						Volume []float32 `json:"volume"`
						Close  []float32 `json:"close"`
						Open   []float32 `json:"open"`
					} `json:"quote"`
				} `json:"indicators"`
			} `json:"result"`
			Error string `json:"error"`
		} `json:"chart"`
	}
)

// Get .
func (api yahooApi) Get(stock shared.StockConfig) shared.StockConfigRuntime {
	scr := shared.StockConfigRuntime{
		StockConfig: stock,
	}

	req, _ := http.NewRequest("GET", api.getURL(stock.Stock), nil)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		scr.Error = err.Error()
		return scr
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		scr.Error = err.Error()
		return scr
	}

	resBody := yahooApiResponse{}
	if err := json.Unmarshal(body, &resBody); err != nil {
		fmt.Println(string(body))
		scr.Error = err.Error()
		return scr
	}

	if resBody.Chart.Error != "" {
		fmt.Println(resBody.Chart.Error)
		scr.Error = resBody.Chart.Error
		return scr
	}

	if len(resBody.Chart.Result) == 0 {
		fmt.Println("missing resBody.Chart.Result")
		scr.Error = "missing resBody.Chart.Result"
		return scr
	}

	if len(resBody.Chart.Result[0].Indicators.Quote) == 0 {
		fmt.Println("missing resBody.Chart.Result[0].Indicators.Quote")
		scr.Error = "missing resBody.Chart.Result[0].Indicators.Quote"
		return scr
	}

	closes := resBody.Chart.Result[0].Indicators.Quote[0].Close
	opens := resBody.Chart.Result[0].Indicators.Quote[0].Open

	if len(closes) == 0 {
		fmt.Println("missing closes")
		scr.Error = "missing closes"
		return scr
	}

	if len(opens) == 0 {
		fmt.Println("missing opens")
		scr.Error = "missing opens"
		return scr
	}

	cPrice := api.getLastActiveValue(closes)
	lPrice := int(opens[0])

	scr.StockPrice = cPrice
	scr.LastDayPrice = lPrice

	return scr
}

func (api yahooApi) getLastActiveValue(closes []float32) int {
	for i := len(closes) - 1; i >= 0; i-- {
		if closes[i] != 0 {
			return int(closes[i])
		}
	}

	return 0
}

func (yahooApi) getURL(stockCode string) string {
	u := url.URL{
		Scheme: "https",
		Host:   "query1.finance.yahoo.com",
		Path:   fmt.Sprintf("v8/finance/chart/%v.JK", stockCode),
	}
	q := u.Query()
	q.Set("q", "golang")
	q.Set("region", "US")
	q.Set("lang", "en-US")
	q.Set("includePrePost", "false")
	q.Set("interval", "1h")
	q.Set("useYfid", "true")
	q.Set("range", "1d")
	q.Set("corsDomain", "finance.yahoo.com")
	q.Set(".tsrc", "finance")

	u.RawQuery = q.Encode()

	return u.String()
}
