package stockprice

import "golang-tray-app/src/shared"

// Getter .
type Getter interface {
	Get(shared.StockConfig) (stock shared.StockConfigRuntime)
}

// BulkGetter .
type BulkGetter interface {
	BulkGet([]shared.StockConfig) (stock []shared.StockConfigRuntime)
}
