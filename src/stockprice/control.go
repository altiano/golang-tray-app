package stockprice

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"golang-tray-app/src/shared"
)

const packageName = "stockprice"
const title = packageName + ".Control()"

// Control .
func Control(cnator *shared.Coordinator) {
	// welcome
	fmt.Println(title)
	config := shared.NewConfig()
	list := []shared.StockConfig{}
	var interval <-chan time.Time
	lock := sync.Mutex{}

	for {
		var ctx context.Context
		var cancel context.CancelFunc

		select {
		case list = <-cnator.StockList:
			fmt.Println(packageName + " <-cnator.StockList")

			lock.Lock()
			if cancel != nil {
				cancel()
			}
			lock.Unlock()

			interval = time.After(0)
		case <-interval:
			processDone := make(chan struct{})
			ctx, cancel = context.WithCancel(context.Background())

			go func() {
				done := make(chan struct{})
				go func() {
					process(ctx, cnator, list)
					done <- struct{}{}
				}()

				interval = time.After(config.ScrapInterval)
				cnator.LastBatchUpdate <- time.Now()
				<-done

				lock.Lock()
				cancel = nil
				lock.Unlock()

				processDone <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				go func() {
					<-processDone
				}()
			case <-processDone:
			}

		}
	}
}

func process(ctx context.Context, cnator *shared.Coordinator, list []shared.StockConfig) {
	var api BulkGetter = SnApi{}

	if len(os.Args) > 1 {
		api = mockApi{}
	}

	concurentLimit := 5
	iter := 0

	for iter*concurentLimit < len(list) {
		start := iter * concurentLimit
		until := ((iter + 1) * concurentLimit)
		if until > len(list) {
			until = len(list)
		}
		listWave := list[start:until]
		stockConfigRuntimes := make(chan []shared.StockConfigRuntime)

		go func(listWave []shared.StockConfig) {
			stockConfigRuntimes <- api.BulkGet(listWave)
		}(listWave)

		select {
		case <-ctx.Done():
			<-stockConfigRuntimes
			return

		case scRuntimes := <-stockConfigRuntimes:
			for i := range listWave {
				item := listWave[i]
				stockConfigRuntime := scRuntimes[i]

				// NOTE
				// THIS UPDATE should only happen once
				// or should be another way
				if item.LastDayPrice != stockConfigRuntime.LastDayPrice {
					req := shared.UpdateStockRequestOrigin{
						StockName: stockConfigRuntime.Stock,
						UpdatedData: map[string]interface{}{
							"LastDayPrice": stockConfigRuntime.LastDayPrice,
						},
						ErrRes: make(chan error),
					}

					cnator.UpdateStockOrigin <- req
					<-req.ErrRes
				}

				cnator.UpdateStock <- shared.UpdateStockRequest{
					Stock: stockConfigRuntime.Stock,
					Price: stockConfigRuntime.StockPrice,
					Error: stockConfigRuntime.Error,
				}
			}
		}

		// next
		iter++
		<-time.After(1 * time.Second)
	}
}
