module golang-tray-app

go 1.16

require (
	github.com/asdine/storm v2.1.2+incompatible // indirect
	github.com/getlantern/appdir v0.0.0-20180320102544-7c0f9d241ea7 // indirect
	github.com/getlantern/systray v1.1.0 // indirect
	github.com/getlantern/uuid v1.2.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/lxn/walk v0.0.0-20191113135339-bf589de20b3c // indirect
	github.com/lxn/win v0.0.0-20191106123917-121afc750dd3 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	gitlab.com/altiano/goutils v0.0.0-20210412152713-85905fa0c734 // indirect
	go.etcd.io/bbolt v1.3.5 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
